#ASSIGN VARIABLES
pub_ip="$(wget -q -O - http://169.254.169.254/latest/meta-data/public-ipv4)"
ec2_id="$(wget -q -O - http://169.254.169.254/latest/meta-data/instance-id)"
echo $pub_ip
echo $ec2_id
dir_name="$(ls /var/ | grep app)"
app_name="$(ls /var/*app/)"
# CHANGE NAME ACC TO REQUIREMENT
sudo mv /var/${dir_name}/${app_name} /var/${dir_name}/${pub_ip}-${ec2_id}-${app_name}
app_new_name="$(ls /var/*app/)"

#DEPLOY
aws s3 cp /var/${dir_name}/${app_new_name} s3://$1
